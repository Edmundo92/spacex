## 🚀 Tecnologia

- React

## Api
- [Json-Server](https://github.com/typicode/json-server)

## 🔥 Instale & Execute a Api
- Instalar globalmente (ou local) a biblioteca json-server  `npm install -g json-server`
- Navegue até a pasta de nome "api" e execute o comando descrito abaixo
- json-server --watch db.json --delay 2000 --port 5000

## ✋🏻 Pre-requisitos

- [Node.js](https://nodejs.org/)
- [React.js](https://pt-br.reactjs.org/)

## 🔥 Instale & Execute

1. Clone o repositório;
2. Abra o repositorio clonado;
3. Execute `npm install` para instalar as dependêcias do projeto;
4. Execute `npm run start` para iniciar o app;
5. Abra [http://localhost:3000](http://localhost:3000) para visualizá-lo no navegador.

## Testes
- Execute `npm run test` 