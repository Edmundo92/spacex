import faker from 'faker';
import { RemoteDestroyUser } from '../data/usecases/remote-destroy-user';
import { HttpClientSpy } from './mocks/mock-http';

describe("RemoteRemoveUser", () => {
    test("Should call HttpClient with correct values", async () => {
        const url = `${faker.internet.url()}/${faker.random.number()}`;

        const httpClientSpy = new HttpClientSpy<any>();
        const sut = new RemoteDestroyUser(url, httpClientSpy)

        await sut.destroy();

        expect(httpClientSpy.url).toBe(url);
        expect(httpClientSpy.method).toBe("delete");
        
    })
})