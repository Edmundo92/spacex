import faker from 'faker';
import { HttpClientSpy } from './mocks/mock-http';
import { HttpStatusCode } from '../infra/http/http-client';
import { AuthAccess } from '../domain/usecases/auth';
import { RemoteAuth } from '../data/usecases/remote-auth';

describe("RemoteAuth", () => {
    test("Should call HttpClient with correct values", async () => {
        const url = faker.internet.url();

        const mockParams = {
            email: faker.internet.email(),
            password: faker.internet.password(),
        };
        
        const mockReponseParams = {
            id: faker.random.number(),
            nome: faker.name.findName(),
            token: "$2y$12$sY7X4aXEGtVxq2FCD1Bcoe/rHLNznE6IOWvF0DNX5hZeSq4kWnYya",
            email: faker.internet.email(),
            password: faker.internet.password(),
        };

        const httpClientSpy = new HttpClientSpy<AuthAccess>();
        const sut = new RemoteAuth(url, httpClientSpy)

        // httpClientSpy.response = {
        //     body: mockReponseParams,
        //     statusCode: HttpStatusCode.ok,
        // }

        await sut.login(mockParams);

        expect(httpClientSpy.url).toBe(url);
        expect(httpClientSpy.method).toBe("get");
        //expect(httpClientSpy.body).toEqual(mockReponseParams);
        
    })
})