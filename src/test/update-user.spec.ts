import faker from 'faker';
import { HttpClientSpy } from './mocks/mock-http';
import { mockAddUser } from './mocks/mock-add-user';
import { UserParams } from '../domain/usecases/add-user';
import { RemoteUpdateUser } from '../data/usecases/remote-update-user';

describe("RemoteUpdateUser", () => {
    test("Should call HttpClient with correct values", async () => {
        const url = faker.internet.url();

        const mockParams = {
            id: faker.random.number(),
            nome: faker.name.findName(),
            email: faker.internet.email(),
            cpf: faker.internet.ip(),
            endereco: {
                bairro: faker.name.findName(),
                cep: Number(faker.internet.ip()),
                cidade: faker.name.findName(),
                numero: faker.random.number(),
                rua: faker.name.findName()
            }
        };

        const httpClientSpy = new HttpClientSpy<UserParams>();
        const sut = new RemoteUpdateUser(url, httpClientSpy)

        await sut.update(mockParams);

        expect(httpClientSpy.url).toBe(url);
        expect(httpClientSpy.method).toBe("put");
        expect(httpClientSpy.body).toEqual(mockParams);
        
    })
})