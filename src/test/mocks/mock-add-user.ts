import { UserParams } from "../../domain/usecases/add-user";
import faker from "faker"

export const mockAddUser = (): UserParams => ({
    nome: faker.name.findName(),
    email: faker.internet.email(),
    cpf: faker.internet.ip(),
    endereco: {
        bairro: faker.name.findName(),
        cep: Number(faker.internet.ip()),
        cidade: faker.name.findName(),
        numero: faker.random.number(),
        rua: faker.name.findName()
    }
})