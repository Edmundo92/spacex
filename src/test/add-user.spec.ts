import faker from 'faker';
import { RemoteAddUser } from "../data/usecases/remote-add-user";
import { HttpClientSpy } from './mocks/mock-http';
import { UserParams } from '../domain/usecases/add-user';
import { HttpStatusCode } from '../infra/http/http-client';

describe("RemoteAddUser", () => {
    test("Should call HttpClient with correct values", async () => {
        const url = faker.internet.url();

        const mockParams = {
            nome: faker.name.findName(),
            email: faker.internet.email(),
            cpf: faker.internet.ip(),
            endereco: {
                bairro: faker.name.findName(),
                cep: Number(faker.internet.ip()),
                cidade: faker.name.findName(),
                numero: faker.random.number(),
                rua: faker.name.findName()
            }
        };

        const httpClientSpy = new HttpClientSpy<UserParams>();
        const sut = new RemoteAddUser(url, httpClientSpy)

        httpClientSpy.response = {
            statusCode: HttpStatusCode.created,
        }

        await sut.add(mockParams);

        expect(httpClientSpy.url).toBe(url);
        expect(httpClientSpy.method).toBe("post");
        expect(httpClientSpy.body).toEqual(mockParams);
        
    })
})