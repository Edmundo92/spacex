import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Login from '../presentation/pages/Login';
import Admin from '../presentation/pages/Admin';
import PrivateRoute from './private-route';
import GuardRoute from './guard-route';

const Router: React.FC = () => {
 
  return (
    <BrowserRouter>
        <Switch>
            <GuardRoute path="/" exact component={Login} />
            <PrivateRoute path="/admin" component={Admin} />
        </Switch>
    </BrowserRouter>
  )
}

export default Router
