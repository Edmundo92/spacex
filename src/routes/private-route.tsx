import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAuthenticated } from "../helpers/isAuthenticated";



type PrivateRouteProps = {
  path?: string | string[];
};

const PrivateRoute: React.FC<any> = ({ children, ...rest }) => {

  return isAuthenticated() ? (
      <Route {...rest} />
    ) : (
      <Redirect to="/" />
    )
     
};

export default PrivateRoute;