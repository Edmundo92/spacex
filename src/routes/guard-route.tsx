import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAuthenticated } from "../helpers/isAuthenticated";



type GuardRouteProps = {
  path?: string | string[];
};

const GuardRoute: React.FC<any> = ({ children, ...rest }) => {

  return !isAuthenticated() ? (
      <Route {...rest} />
    ) : (
      <Redirect to="/admin/list" />
    )
     
};

export default GuardRoute;