import React from 'react';
import Router from './routes/route';
import  "./App.css";
import { ToastProvider } from 'react-toast-notifications';

function App() {
  return (
    <ToastProvider>
      <Router />
    </ToastProvider>
  )
}

export default App;
