export type UserParams = {
    id?: number,
    nome: string,
    email: string,
    cpf: string,
    endereco: EnderecoParams    
}

type EnderecoParams = {
    cep: number,
    rua: string,
    numero: number,
    bairro: string,
    cidade: string
}

export interface AddUser {
    add(params: UserParams): Promise<any>
}