import { UserParams } from "./add-user";

export interface UpdateUser {
    update(params: UserParams): Promise<any>
}