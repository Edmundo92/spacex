export interface RemoveUser {
    destroy(id: number): Promise<any>
}