import { UserParams } from "./add-user";

export interface ListUsers {
    list(): Promise<UserParams[]>
}