import { UserParams } from "./add-user";

export type LoginParams = {
    email: string,
    password: string
}

export type AuthAccess = {
    id: number,
    token: string,
    nome: string,
    email: string,
    password: string
}

export interface Authentication {
    login(params: LoginParams): Promise<AuthAccess>
}