import { UserParams } from "./add-user";

export interface GetUser {
    getUser(): Promise<UserParams>
}