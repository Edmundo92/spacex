import { UserParams } from "./add-user";

export type CEPParams = {
    cep: string,
    logradouro: string,
    complemento: string,
    bairro: string,
    localidade: string,
    uf: string,
    ibge: number,
    gia: number,
    ddd: number,
    siafi: number
}

export interface GetCEP {
    getCEP(): Promise<CEPParams>
}