export class AcessDeniedError extends Error {
    constructor() {
        super("Acesso negado!")
        this.name = "AccessDeniedError"
    }
}