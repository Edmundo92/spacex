import { UserParams } from "../../domain/usecases/add-user";
import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { UpdateUser } from "../../domain/usecases/update-user";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteUpdateUser implements UpdateUser {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async update(params: UserParams) : Promise<any> {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "put",
            body: params
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return { ...remoteUserData }
            default: throw new UnexpectedError()
        }
    }
}