import { AddUser, UserParams } from "../../domain/usecases/add-user";
import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { ListUsers } from "../../domain/usecases/list-users";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteListUsers implements ListUsers {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async list() : Promise<UserParams[]> {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "get"        
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return [...remoteUserData]
            default: throw new UnexpectedError()
        }
    }
}