import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { UnexpectedError } from "../../domain/errors/unexpected-error";
import { Authentication, LoginParams, AuthAccess } from "../../domain/usecases/auth";
import { InvalidCredentialsError } from "../../domain/errors/invalid-credentials-error";
import { LocalStorage } from "../../infra/cache/local-storage";

import jwt from "jsonwebtoken"
import bcrypt from "bcryptjs"

export class RemoteAuth implements Authentication {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async login(params: LoginParams) : Promise<AuthAccess> {
        
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "get",
        });

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return httpResponse.body
            case HttpStatusCode.unauthorized: throw new InvalidCredentialsError()
            default: throw new UnexpectedError()
        }
    }
}

export function findUserAdmin(remoteUserData: any, params: LoginParams, fnRedirect: () => void) {
    const CACHE_NAME = "Space-X-Cache"
    const cache = new LocalStorage();

    remoteUserData.map((user: AuthAccess) => {

        if(user.email === params.email && verifyPassword(params.password, user.password)) {
            const { password, ...rest } = user
            cache.set(CACHE_NAME, { ...rest, token: generateToken({ email: rest.email }) });
            fnRedirect()
        }
    })


}

function generateToken(value: object) {
    const PRIVATE_TOKEN = "SPACEX_KEY";
    const token = jwt.sign(value, PRIVATE_TOKEN);
    return token;
}

async function verifyPassword(hashPassword: string, password: string) {
    const match = await bcrypt.compare(password, hashPassword);
    return match
}

// function updateAccessToken(token: string) {
//     const httClient = new HttpClient();
// }