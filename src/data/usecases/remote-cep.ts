import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { GetCEP, CEPParams } from "../../domain/usecases/cep";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteCEP implements GetCEP {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async getCEP() : Promise<CEPParams> {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "get"        
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return {...remoteUserData}
            default: throw new UnexpectedError()
        }
    }
}