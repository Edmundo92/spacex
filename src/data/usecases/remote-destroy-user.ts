import { AddUser, UserParams } from "../../domain/usecases/add-user";
import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { RemoveUser } from "../../domain/usecases/destroy-user";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteDestroyUser implements RemoveUser {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async destroy() {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "delete"        
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return {...remoteUserData}
            default: throw new UnexpectedError()
        }
    }
}