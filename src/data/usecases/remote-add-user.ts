import { AddUser, UserParams } from "../../domain/usecases/add-user";
import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteAddUser implements AddUser {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async add(params: UserParams) : Promise<UserParams> {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "post",
            body: params
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.created: return httpResponse.body
            default: throw new UnexpectedError()
        }
    }
}