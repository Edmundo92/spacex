import { AddUser, UserParams } from "../../domain/usecases/add-user";
import { HttpClient, HttpStatusCode } from "../../infra/http/http-client";
import { GetUser } from "../../domain/usecases/user";
import { UnexpectedError } from "../../domain/errors/unexpected-error";

export class RemoteGetUser implements GetUser {

    constructor(
        private readonly url: string,
        private readonly httClient: HttpClient
    ) {}

    async getUser() : Promise<UserParams> {
        const httpResponse = await this.httClient.request({
            url: this.url,
            method: "get"        
        });

        const remoteUserData = httpResponse.body;

        switch(httpResponse.statusCode) {
            case HttpStatusCode.ok: return {...remoteUserData}
            default: throw new UnexpectedError()
        }
    }
}