import axios, { AxiosResponse } from "axios"
import { HttpClient, HttpRequest, HttpResponse } from "./http-client";

export class AxiosHttpClient implements HttpClient {
    async request(data: HttpRequest): Promise<HttpResponse> {

        let axiosRespone: AxiosResponse

        try {
            axiosRespone = await axios.request({
                url: data.url,
                method: data.method,
                data: data.body,
                headers: data.headers
            })
        } catch(error) {
            axiosRespone = error.response
        }

        return {
            statusCode: axiosRespone.status,
            body: axiosRespone.data
        }
    }
}