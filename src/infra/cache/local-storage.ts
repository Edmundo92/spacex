import { SetStorage, GetStorage, RemoveStorage } from "./storage";

export class LocalStorage implements SetStorage, GetStorage, RemoveStorage {

    set(key: string, value: object): void {
        if(value) {
            localStorage.setItem(key, JSON.stringify(value));
        } else {
            localStorage.removeItem(key);
        }
    }

    get(key: string): any {
        return JSON.parse(localStorage.getItem(key) || "");
    }

    remove(key: string): void {
        localStorage.removeItem(key)
    }
}