export function email(value: string) {
    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    return emailRegex.test(value);
}

export function required(value: string) {
    return value.length > 0 ? true : false;
}

export function minLength(value: string, min: number) {
    return value.length < min ? true : false
} 