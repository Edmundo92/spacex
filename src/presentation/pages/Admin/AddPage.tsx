import React, { useState, useEffect, useRef } from "react";
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import { useHistory, useParams, useLocation } from 'react-router-dom'
import { UserParams } from "../../../domain/usecases/add-user";
import { RemoteAddUser } from "../../../data/usecases/remote-add-user";
import { AxiosHttpClient } from "../../../infra/http/axios-http-client";
import { RemoteUpdateUser } from "../../../data/usecases/remote-update-user";
import { RemoteGetUser } from "../../../data/usecases/remote-get-user";
import { RemoteCEP } from "../../../data/usecases/remote-cep";
import { email } from "../../../validators";
import { mask } from "../../../helpers/mask"
import { useToasts } from "react-toast-notifications";
import { makeApiUrl } from "../../../main/url-factory";

export default function AddPage() {

    const history = useHistory();
    let params = useParams<any>();
    const location = useLocation()
    const { addToast } = useToasts();
    const numberRef = useRef<HTMLHeadingElement>(null);
    const [register, setRegister] = useState<any>({
        nome: "",
        cpf: "",
        email: "",
        cep: "",
        rua: null,
        numero: null,
        bairro: "",
        cidade: ""
    });

    const [loading, setLoading] = useState(false)
    const [cepLoading, setCepLoading] = useState(false)

    const [errorEmailMessage, setErrorEmailMessage] = useState("")

    const registerUser = new RemoteAddUser(makeApiUrl("/usuarios"), new AxiosHttpClient())

    function getCEP(value: string) {
        const cep = new RemoteCEP(`http://viacep.com.br/ws/${value}/json/`, new AxiosHttpClient())
        return cep.getCEP()
    }

    function validateAndCallCep(value: string) {
        if (value.length === 8) {
           try {
            setCepLoading(true)
            getCEP(value).then((res) => {
                const { bairro, logradouro, localidade } = res;
                setRegister({
                    bairro,
                    rua: logradouro,
                    cidade: localidade
                });
            })
           } catch(error) {
               setCepLoading(false)
           } finally {
            setCepLoading(false)
           }
        }
    }

    function handleChange(e: any) {
        const { value, name } = e.target;

        if (name === "cep") {
            validateAndCallCep(mask.removeMask(value))
            setRegister((old: any) => ({ ...old, [name]: mask.cep(value) }))
            return
        }

        if (name === "cpf") {
            setRegister((old: any) => ({ ...old, [name]: mask.cpf(value) }))
            return
        }

        if (name === "email") {
            if (!email(value)) setErrorEmailMessage("E-mail inválido")
            if (email(value)) {
                setErrorEmailMessage("")
            }
        }

        setRegister((old: any) => ({ ...old, [name]: value }))
    }

    function redirectToList() {
        setTimeout(() => {
            history.push("/admin/list")
        }, 600)
    }

    function handleSubmit(e: any) {
        const { nome, cpf, email, cep, rua, numero, bairro, cidade } = register;

        setLoading(true)

        if (params && params.id) {
            let updateUser = new RemoteUpdateUser(makeApiUrl(`/usuarios/${params.id}`), new AxiosHttpClient())
            updateUser.update({
                nome,
                cpf: mask.removeMask(cpf),
                email,
                endereco: {
                    cep: mask.removeMask(cep),
                    rua, numero, bairro, cidade
                }
            }).then((res) => {
                setLoading(false)
                addToast("Dados atualizados com sucesso!", { appearance: "success" })
                redirectToList()
            }).catch((error) => {
                setLoading(false)
                console.log(error)
            })
        } else {

            registerUser.add({
                nome, cpf, email,
                endereco: {
                    cep, rua, numero, bairro, cidade
                }
            }).then((resp) => {
                setLoading(false)
                addToast("Usuário registrado com sucesso!", { appearance: "success" })
                redirectToList()
            }).catch((error) => {
                setLoading(false)
            })
        }
    }

    function getUser() {
        let getUser = new RemoteGetUser(makeApiUrl(`/usuarios/${params.id}`), new AxiosHttpClient());
        getUser.getUser().then((res) => {
            const { endereco, ...rest } = res;
            setRegister({
                ...rest,
                ...endereco
            })
        })
    }

    useEffect(() => {
        if (params && params.id) {
            getUser()
        }
    }, [location])

    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 600 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Registrar usuário
                 </Header>
                <Form onSubmit={handleSubmit} size='large'>

                    <Form.Input required value={register.nome} name="nome" onChange={handleChange} fluid icon='user' iconPosition='left' placeholder='Nome' />
                    <Form.Input error={errorEmailMessage.length > 0 ? errorEmailMessage : null} required value={register.email} name="email" onChange={handleChange} fluid icon='mail' iconPosition='left' placeholder='E-mail' />
                    <Form.Input required value={register.cpf} name="cpf" onChange={handleChange} fluid icon='id card' iconPosition='left' placeholder='CPF' />
                    <Grid columns='equal'>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Input required loading={cepLoading} value={register.cep} name="cep" onChange={handleChange} fluid icon='map' iconPosition='left' placeholder='cep' />
                            </Grid.Column>
                            <Grid.Column>
                                <Form.Input required value={register.rua} name="rua" onChange={handleChange} fluid icon='street view' iconPosition='left' placeholder='rua' />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Input required value={register.bairro} name="bairro" onChange={handleChange} fluid icon='map' iconPosition='left' placeholder='bairro' />
                            </Grid.Column>
                            <Grid.Column>
                                <Form.Input required ref={numberRef} value={register.numero} name="numero" onChange={handleChange} fluid icon='home' iconPosition='left' placeholder='numero' />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Form.Input required value={register.cidade} name="cidade" onChange={handleChange} fluid icon='map' iconPosition='left' placeholder='cidade' />

                    <Button loading={loading} type="submit" color='teal' fluid size='large'>
                        { params && params.id ?  "Salvar" : "Guardar" }
                    </Button>

                </Form>
            </Grid.Column>
        </Grid>
    )
}