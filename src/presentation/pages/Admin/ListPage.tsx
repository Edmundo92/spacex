import React, { useEffect, useState } from "react";
import { useHistory, useParams } from 'react-router-dom'
import ListContainer from "../../containers/admin/ListContainer";
import { RemoteListUsers } from "../../../data/usecases/remote-list-users";
import { AxiosHttpClient } from "../../../infra/http/axios-http-client";
import { UserParams } from "../../../domain/usecases/add-user";
import { RemoteUpdateUser } from "../../../data/usecases/remote-update-user";
import { RemoteDestroyUser } from "../../../data/usecases/remote-destroy-user";
import Error from "../../components/Error";
import { Grid } from "semantic-ui-react";
import { useToasts } from "react-toast-notifications";
import { makeApiUrl } from "../../../main/url-factory";


export default function ListPage() {
    const history = useHistory();
    const { addToast } = useToasts();
    const [users, setUsers] = useState<any>([])
    const [error, setError] = useState<boolean>(false)
    const [isLoading, setisLoading] = useState<boolean>(false)
    const [search, setSearch] = useState<any>({
        isLoading: false,
        results: [],
        value: ""
    })
    
    const getUsers = new RemoteListUsers(makeApiUrl("/usuarios"), new AxiosHttpClient())

    function allUsers() {
        setisLoading(true)
        getUsers.list().then((res) => {
            setUsers(res)
            setError(false)
            setisLoading(false)
        })
        .catch(error => {
            setError(true)
            setisLoading(false)
        })
    }

    useEffect(() => {
        allUsers()
    }, [])

    function handleUpdate(user: UserParams) {
        history.push(`/admin/add/${user.id}`)
    }
    
    function handleDelete(user: UserParams) {
        const destroyUser = new RemoteDestroyUser(makeApiUrl(`/usuarios/${user.id}`), new AxiosHttpClient())
        destroyUser.destroy().then((res) => {
            addToast("Usuário removido com sucesso!", { appearance: "success" })
            allUsers()
        })
    }

    function handleSearch(e: any) {
        const { value } = e.target;

        if(value.length > 2) {
            const users = new RemoteListUsers(makeApiUrl(`/usuarios?nome_like=${value}`), new AxiosHttpClient())
            users.list().then((res) => {
                setUsers(res)
            })
        } 
        
        if(value.length === 0){
            allUsers()
        }
    }

    return (
       <>
        {
            error ? (
                <Grid centered columns="1">
                    <Grid.Column width={12}>
                        <Error reload={() => allUsers()} />
                    </Grid.Column>
                </Grid>
            ) : (
                <ListContainer  
                    handleUpdate={handleUpdate}
                    handleDelete={handleDelete}
                    users={users} 
                    search={search}
                    isLoading={isLoading}
                    handleSearch={handleSearch}
                />
            )
        }
       </>
    )
}