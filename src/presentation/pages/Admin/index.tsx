import React from "react";
import { BrowserRouter, Switch, Route, useHistory } from 'react-router-dom'

import {
    Container,
    Divider,
    Dropdown,
    Grid,
    Icon,
    Image,
    List,
    Menu,
    Segment,
    Visibility,
} from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

import SPACEX from "../../../static/images/spacex.png"

import ListPage from "./ListPage";
import AddPage from "./AddPage";
import { Header, ListNav, Item } from "../../styles/admin.style";
import { LocalStorage } from "../../../infra/cache/local-storage";

export default function Admin() {
    const history = useHistory();

    function logout() {
        let cache = new LocalStorage();
        cache.remove("Space-X-Cache");
        history.push("/")
    }

    return (
        <>
            <Header>
                <div>
                 <img className="logo-spacex" width="120" height="25" src={SPACEX} alt="SPACEX"/>
                </div>
                <ListNav>
                    <Item>
                        <NavLink activeClassName="active-route" to="/admin/add">Cadastrar</NavLink>
                    </Item>
                    <Item>
                        <NavLink activeClassName="active-route" to="/admin/list">Listar</NavLink>
                    </Item>
                    <Item onClick={logout}>Sair</Item>
                </ListNav>
            </Header>

            <Switch>
                <Route path="/admin/add/:id" component={AddPage} />
                <Route path="/admin/add" component={AddPage} />
                <Route path="/admin/list" component={ListPage} />
            </Switch>
        </>
    )
}