import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Container, Content, FirstColumn, SecondColumn, FormContent } from "../styles/login.style"

import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import { RemoteAuth, findUserAdmin } from "../../data/usecases/remote-auth";
import { AxiosHttpClient } from "../../infra/http/axios-http-client";
import { LoginParams } from "../../domain/usecases/auth";
import { email, required, minLength } from "../../validators";

import IMG_HOME from "../../static/images/img.svg"
import LOGO from "../../static/images/spacex.png"
import { makeApiUrl } from "../../main/url-factory";


export default function Login() {
  const history = useHistory()
  const [params, setParams] = useState<LoginParams>({
    email: "",
    password: ""
  })

  const auth = new RemoteAuth(makeApiUrl("/admin"), new AxiosHttpClient());

  const [loading, setLoading] = useState(false)

  const [errorEmailMessage, setErrorEmailMessage] = useState("")
  const [errorPasswordMessage, setErrorPasswordMessage] = useState("")

  function redirect() {
    setLoading(false)
    history.push("admin/list");
  }

  function handleSubmit() {

    if(params.email.length === 0) setErrorEmailMessage("Campo obrigatório")
    if(params.password.length === 0) setErrorPasswordMessage("Campo obrigatório")

    if (!errorEmailMessage && !errorPasswordMessage && params.email && params.password) {
      setLoading(true);
      auth.login(params)
      .then((res) => {
        findUserAdmin(res, params, redirect)
        setLoading(false)
      }).catch((error) => {
        setLoading(false)
      })
    }
  }

  function handleChange(e: any) {
    const { value, name } = e.target;

    if (name === "email") {
      if (!email(value)) setErrorEmailMessage("E-mail inválido")
      if (!required(value)) setErrorEmailMessage("Campo obrigatório")
      if (email(value) && required(value)) {
        setErrorEmailMessage("")
      }
    }

    if (name === "password") {
      if (!required(value)) setErrorPasswordMessage("Campo obrigatório")
      if (minLength(value, 4)) setErrorPasswordMessage("Deve ter no minimo 4 digitos")
      if (!minLength(value, 4) && required(value)) {
        setErrorPasswordMessage("")
      }
    }

    setParams((old: LoginParams) => ({ ...old, [name]: value }))
  }

  return (
    <Container>
      <Content>
        <FirstColumn>
          <>
            <Image className="home" src={IMG_HOME} />
            <h1>Seja Bem-Vindo</h1>
          </>
        </FirstColumn>
        <SecondColumn>
          <FormContent>
          
          <img className="logo-login" src={LOGO} alt="SpaceX" />
          <Form onSubmit={handleSubmit}>
              <Form.Input className="input" error={errorEmailMessage.length > 0 ? errorEmailMessage : null} name="email" onChange={(handleChange)} fluid icon='user' iconPosition='left' placeholder='E-mail address' />
              <Form.Input className="input"
                name="password"
                onChange={(handleChange)}
                fluid
                icon='lock'
                iconPosition='left'
                placeholder='Password'
                type='password'
                error={errorPasswordMessage.length > 0 ? errorPasswordMessage : null}
              />

              <Button loading={loading} circular type="submit" color='teal' fluid size='medium'>
                Entrar
              </Button>
          </Form>
          </FormContent>
        </SecondColumn>
      </Content>
    </Container>
  )
}