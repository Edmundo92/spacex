import React from "react"
import { Message, Button } from "semantic-ui-react"

type MessageProps = {
    reload?: () => void
}

export default function Error({ reload }: MessageProps) {

    return (
        <Message error style={{
            marginTop: 15
        }}>
            <Message.Header>Algo de errado aconteceu!</Message.Header>
            <p>Por favor, verifique a sua conexão e tente novamente.</p>

            <Button content="Tentar novamente" onClick={() => reload()} />
        </Message>
    )
}