import styled, { css } from 'styled-components'

export const ListNav = styled.ul`
display: flex;
`
export const Item = styled.li`
    cursor: pointer;
    padding: 0 15px;
    list-style: none;
    
    a {
        color: black;
        font-size: 14px;
        text-decoration: none;
    }

`

export const Header = styled.header`
    display: flex;
    height: 50px;
    width: 100%;
    background-color: #58af9b;
    justify-content: space-between;
    padding: 0 25px;
    align-items: center;
    
    .logo-spacex {
        display: none;
    }

    @media (min-width: 500px) {
        .logo-spacex {
            display: block;
        }
    }
`

