import styled, { css } from 'styled-components'

export const Container = styled.div`
    
`

export const Content = styled.div`
display: flex;
    background-color: #fff;
    border-radius: 15px;
    height: 100vh;
    justify-content: space-between;
    align-items: center;
    position: relative;
    
    `

export const FirstColumn = styled.div`
    width: 40%;
    height: 100%;
    display: none;
    background-color: #58af9b;
    padding: 0 30px;
    flex-direction: column;
    align-items: center;
    justify-content: center;

 

    @media(min-width: 600px) {
        display: flex;
    }
`
export const SecondColumn = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;

    @media(min-width: 600px) {
        width: 60%;
    }
`

export const FormContent = styled.div `
    width: 70%;
   
    @media(min-width: 820px) {
        width: 50%;
    }

    .logo-login {
        width: 200px;
        margin: 15px auto;
        display: block;
    }
`
export const Image = styled.img`
    width: 80%;

    .home {
    }
`

