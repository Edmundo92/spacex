import React from "react"
import { Table, Icon, Loader, Dimmer, Grid, Search, Input, Segment } from 'semantic-ui-react'
import { UserParams } from "../../../domain/usecases/add-user";

type ListContainerProps = {
    users: any,
    search: any,
    isLoading: boolean,
    handleSearch: (user: any) => void,
    handleUpdate: (user: UserParams) => void,
    handleDelete: (user: UserParams) => void,
}

export default function ListContainer({ users, handleUpdate, handleDelete, search, handleSearch, isLoading }: ListContainerProps) {

    return (
        <>
            <Grid centered columns="1">
                <Grid.Column width={12}>
                    <Input
                        fluid
                        icon={<Icon name="search" inverted circular link />}
                        loading={search.isLoading}
                        onChange={handleSearch}
                        placeholder="Procurar pelo nome..."
                        style={{
                            marginTop: 25
                        }}
                    />
                </Grid.Column>
                <Grid.Column width={12}>

                    <Table compact>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Nome</Table.HeaderCell>
                                <Table.HeaderCell>CPF</Table.HeaderCell>
                                <Table.HeaderCell>Email</Table.HeaderCell>
                                <Table.HeaderCell>Cidade</Table.HeaderCell>
                                <Table.HeaderCell></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {
                                users &&
                                users.map((el: UserParams) => (
                                    <Table.Row>
                                        <Table.Cell>{el.nome}</Table.Cell>
                                        <Table.Cell>{el.cpf}</Table.Cell>
                                        <Table.Cell>{el.email}</Table.Cell>
                                        <Table.Cell>{el.endereco.cidade}</Table.Cell>
                                        <Table.Cell>
                                            <Icon circular color="teal" name="pencil alternate" onClick={() => handleUpdate(el)} />
                                            <Icon circular color="red" name="trash" onClick={() => handleDelete(el)} />
                                        </Table.Cell>
                                    </Table.Row>
                                ))
                            }


                        </Table.Body>
                    </Table>
                    {
                        users.length === 0 && (
                            <p className="register-message">Nenhum registro cadastrado</p>
                        )
                    }

                    {
                        isLoading && (
                            <Dimmer active inverted>
                                <Loader size="medium">Carregando</Loader>
                            </Dimmer>
                        )
                    }
                </Grid.Column>
            </Grid>
        </>
    )
}