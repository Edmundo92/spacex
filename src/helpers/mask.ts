function cep(value: string) {
    return value.toString()
    .toString()
    .replace(/\D/g, "")
    .replace(/(\d{5})(\d)/, "$1-$2")
    .replace(/(-\d{3})\d+?$/, "$1");
}
function cpf(value: string) {
    return value.toString()
    .replace(/\D/g, "")
    .replace(/(\d{3})(\d)/, "$1.$2")
    .replace(/(\d{3})(\d)/, "$1.$2")
    .replace(/(\d{3})(\d)/, "$1-$2")

    .replace(/(\d{3})-(\d)(\d{3})/, "$1$2-$3")
    .replace(/(-\d{2})\d+?$/, "$1");
}

function removeMask(value: any) {
    if(typeof value === "number") {
        return parseInt(value.toString().replace(/\D/g, ""))
    } else {
        return value.toString().replace(/\D/g, "")
    }
} 

export const mask = {
    cep,
    cpf,
    removeMask
}