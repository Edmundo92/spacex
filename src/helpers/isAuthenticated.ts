export const isAuthenticated = () => {
    const CACHE_NAME = "Space-X-Cache"

    const data = JSON.parse(localStorage.getItem(CACHE_NAME) || "{}");
    return (
      localStorage.getItem(CACHE_NAME) !== null && data["token"].length > 0
    );
  };